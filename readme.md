## Đặt cơm tự động

Demo chạy thử xem file `auto launch.mov`

## Cấu hình

- Ngày ăn cơm

```
$days = [
	2,3,4,5,6 // ăn cơm thứ 2,3,4,5,6
];
```

- Tên truy cập

```
define( "ERP_USER", "username");
define( "ERP_PASSWORD", "password");
```

## Chạy thử 

```
php index.php
```

## Đặt tự động

Sử dụng crontab đặt chạy tự động vào giờ bạn hay bật máy, 
Có thể đặt nhiều hơn 1 lần.
Ví dụ đặt chạy tự động 11h hàng ngày

```
* 11 * * * /path/to/php /path/to/index.php
```