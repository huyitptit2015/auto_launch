<?php
/**
 * Created by PhpStorm.
 * User: hocvt
 * Date: 4/4/18
 * Time: 00:34
 */

function login($user, $pass){
	global $client;
	
	show_notification( "Đang đăng nhập " . $user . "....");
	
	$response = $client->get( 'https://erp.nhanh.vn/user/signin');
	$content = $response->getBody()->getContents();
	$matched = preg_match("/id=\"csrf\"\svalue=\"([\w\-]+)\"/", $content, $matches);
	if(!$matched){
		show_notification( "Lỗi đăng nhập ...", true);
		return false;
	}
	$token = $matches[1];
	$data = [
		'csrf' => $token,
		'username' => $user,
		'password' => $pass,
	];
	$response = $client->post( "https://erp.nhanh.vn/user/signin", [
		'form_params' => $data
	]);
	$content = $response->getBody()->getContents();
	
	if(strpos( $content, "href=\"/profile\"")){
		show_notification( "Đã đăng nhập ....");
	}else{
		show_notification( "Không thể đăng nhập ....", false);
		die();
	}


}

function check_today(){
	global $client;
	$today = date('Y-m-d');
	$today = str_replace( "-", '\-', $today);
	$response = $client->get( "https://erp.nhanh.vn/hrm/lunch/add");
	$content = $response->getBody()->getContents();
	$matched = preg_match( "/applyDated.+" . $today . "/m", $content);
	if($matched){
		show_notification( "Hôm nay bạn đã đặt cơm ....");
	}else{
		show_notification( "Hôm nay bạn chưa đặt cơm ....", false);
	}
}

function reg_tomorrow(){
	global $next_day, $client;
	$data = [
		'bookDate' => [
			$next_day
		]
	];
	$response = $client->post( "https://erp.nhanh.vn/hrm/lunch/add", [
		'form_params' => $data
	]);
	$content = $response->getBody()->getContents();
	if(strpos( $content, "\"code\":1")){
		show_notification( "Đã đặt cơm cho ngày tiếp theo (" . $next_day . ") ....");
	}else{
		show_notification( "Lỗi đặt cơm cho ngày tiếp theo (" . $next_day . ") ....", false);
	}
}

function show_notification($message, $info = true){
	global $notifier;
	// Create your notification
	$notification =
		(new \Joli\JoliNotif\Notification())
			->setTitle('Auto launch register')
			->setBody($message)
			->setIcon(__DIR__ . ($info ? "/icons/info.png" : "/icons/error.png"))
//			->addOption('subtitle', 'This is a subtitle') // Only works on macOS (AppleScriptNotifier)
//			->addOption('sound', 'Frog') // Only works on macOS (AppleScriptNotifier)
	;

// Send it
	$notifier->send($notification);
}